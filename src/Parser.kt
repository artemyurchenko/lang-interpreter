typealias TT = TokenType

class Parser(text: String) {
    private val lexer: Lexer = Lexer(text)
    private var ctoken: Token = lexer.fetch()
    private var stateStack = mutableListOf<Token>()
    private var accepted: Token = ctoken

    fun parse(): Program {
        return parseProgram()
    }

    private fun parseProgram(): Program {
        val functions = parseMany({ parseFunction().also { expect(TT.EOL) } }, { true })
        val last = parseExpression()
        accept(TT.EOL)
        expect(TT.EOF)
        val program = Program(last)
        functions.forEach(program::add)
        return program
    }

    private fun parseFunction(): Function {
        val funcName = parseIdentifier()
        expect(TT.LPAREN)
        val params = parseSome(this::parseIdentifier, { accept(TT.COMMA) })
        expect(TT.RPAREN)
        expect(TT.EQ_LBRACE)
        val expr = parseExpression()
        expect(TT.RBRACE)
        val function = Function(funcName, expr)
        params.forEach(function::add)
        return function
    }

    private fun parseIdentifier(): Identifier {
        expect(TT.IDENT)
        return Identifier(accepted.loc)
    }

    private fun parseExpression(): Expression {
        return parseMaybe(this::parseConstantExpression)
            ?: parseMaybe(this::parseBinaryExpression)
            ?: parseMaybe(this::parseIfExpression)
            ?: parseMaybe(this::parseCallExpression)
            ?: parseIdentifier()
    }

    private fun parseConstantExpression(): ConstantExpression {
        val negate = accept(TT.MINUS)
        expect(TT.NUMBER)
        return ConstantExpression(negate, accepted.loc)
    }

    private fun parseBinaryExpression(): BinaryExpression {
        expect(TT.LPAREN)
        val operand1 = parseExpression()
        val opToken = ctoken
        val operation = parseOperation()
        val operand2 = parseExpression()
        expect(TT.RPAREN)
        return BinaryExpression(operation, opToken.loc.getValue(), operand1, operand2)
    }

    private fun parseIfExpression(): IfExpression {
        expect(TT.LBRACKET)
        val cond = parseExpression()
        expect(TT.RBRACKET)
        expect(TT.QUEMARK)
        expect(TT.LBRACE)
        val truthCase = parseExpression()
        expect(TT.RBRACE)
        expect(TT.COLON)
        expect(TT.LBRACE)
        val falsityCase = parseExpression()
        expect(TT.RBRACE)
        return IfExpression(cond, truthCase, falsityCase)
    }

    private fun parseCallExpression(): CallExpression {
        val funcName = parseIdentifier()
        expect(TT.LPAREN)
        val arguments = parseSome(this::parseExpression, { accept(TT.COMMA) })
        expect(TT.RPAREN)
        val call = CallExpression(funcName)
        arguments.forEach(call::add)
        return call
    }

    private fun parseOperation(): Operation {
        return when {
            accept(TT.PLUS) -> Int::plus
            accept(TT.MINUS) -> Int::minus
            accept(TT.STAR) -> Int::times
            accept(TT.SLASH) -> Int::div
            accept(TT.PERCENT) -> Int::rem
            accept(TT.LESS) -> Int::less
            accept(TT.MORE) -> Int::more
            else -> {
                expect(TT.EQ)
                Int::eq
            }
        }
    }

    private fun <T> parseMaybe(parser: () -> T): T? {
        saveState()
        try {
            return parser()
        } catch (error: LangSyntaxError) {
            restoreFromState()
            return null
        } finally {
            discardState()
        }
    }

    private fun <T> parseMany(itemParser: () -> T, sep: () -> Boolean): List<T> {
        val first = parseMaybe(itemParser) ?: return emptyList()
        val list = mutableListOf(first)
        val sepAndList = { parseMaybe { sep(); itemParser() } }
        var next = sepAndList()
        while (next != null) {
            list.add(next)
            next = sepAndList()
        }
        return list
    }

    private fun <T> parseSome(itemParser: () -> T, sep: () -> Boolean): List<T> {
        val first = itemParser()
        val list = mutableListOf(first)
        if (sep()) {
            val rest = parseMany(itemParser, sep)
            list.addAll(rest)
        }
        return list
    }

    private fun accept(type: TokenType): Boolean {
        if (ctoken.type == type) {
            accepted = ctoken
            ctoken = lexer.fetch()
            return true
        } else {
            return false
        }
    }

    private fun expect(type: TokenType) {
        if (!accept(type)) {
            throw LangSyntaxError(ctoken.loc)
        }
    }

    private fun saveState() {
        stateStack.add(ctoken)
        lexer.backup()
    }

    private fun discardState() {
        if (stateStack.isNotEmpty()) {
            stateStack.removeAt(stateStack.count() - 1)
        }
    }

    private fun restoreFromState() {
        ctoken = stateStack.lastOrNull() ?: ctoken
        lexer.restore()
    }
}

private fun Int.less(b: Int) = if (this < b) 1 else 0
private fun Int.more(b: Int) = if (this > b) 1 else 0
private fun Int.eq(b: Int) = if (this == b) 1 else 0
