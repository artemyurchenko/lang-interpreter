import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class ErrorsTest {
    @Test
    fun noParens() {
        val text = "1 + 2 + 3 + 4 + 5"
        checkForException(LangSyntaxError::class.java, text, "SYNTAX ERROR: line #1")
    }

    @Test
    fun parameterListEmpty() {
        val text = "f(x,y)={(x+y)}\ng()={5}\ng()"
        checkForException(LangSyntaxError::class.java, text, "SYNTAX ERROR: line #2")
    }

    @Test
    fun lastLineNotExpression() {
        val text = "f(x,y)={(x+y)}\ng(x)={f(x,x)}"
        checkForException(LangSyntaxError::class.java, text, "SYNTAX ERROR: line #2")
    }

    @Test
    fun noParameter() {
        val text = "f(x)={y}\nf(10)"
        checkForException(ParameterNotFoundException::class.java, text, "PARAMETER NOT FOUND: y:1")
    }

    @Test
    fun noFunction() {
        val text = "g(x)={f(x)}\ng(10)"
        checkForException(FunctionNotFoundException::class.java, text, "FUNCTION NOT FOUND: f:1")
    }

    @Test
    fun moreArgumentsThanRequired() {
        val text = "g(x)={(x+1)}\ng(10,20)"
        checkForException(ArgumentMismatchException::class.java, text, "ARGUMENT NUMBER MISMATCH: g:2")
    }

    @Test
    fun lessArgumentsThanRequired() {
        val text = "g(x,y)={(x*y)}\ng(10)"
        checkForException(ArgumentMismatchException::class.java, text, "ARGUMENT NUMBER MISMATCH: g:2")
    }

    @Test
    fun divisionByZero() {
        val text = "g(a,b)={(a/b)}\ng(10,0)"
        checkForException(LangRuntimeError::class.java, text, "RUNTIME ERROR: (a/b):1")
    }

    private fun checkForException(exceptionClass: Class<out Exception>, text: String, prefix: String?) {
        val exception = assertThrows(exceptionClass, { eval(text) })
        if (prefix != null) {
            assertTrue(
                exception.message!!.startsWith(prefix),
                "'%s' is not prefix of error message:\n%s"
                    .format(prefix, exception.message)
            )
        }
    }

    private fun eval(text: String): Int {
        val program = Parser(text).parse()
        return program.evaluate()
    }
}