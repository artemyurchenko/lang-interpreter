import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ParserTest {
    @Test
    fun justConstant() {
        check("563")
    }

    @Test
    fun justIdentifier() {
        check("idX")
    }

    @Test
    fun simpleArithmetic() {
        check("(((5+4)/3)=3)")
    }

    @Test
    fun ifCondition() {
        check("[((xerz+20)>(20+a))]?{1}:{__}")
    }

    @Test
    fun simpleProgram() {
        check(
            """
            g(x)={(f(x)+f((x/2)))}
            f(x)={[(x>1)]?{(f((x-1))+f((x-2)))}:{x}}
            g(10)
            """.trimIndent()
        )
    }

    private fun check(source: String) {
        val parser = Parser(source)
        val ast = parser.parse()
        val builder = StringBuilder()
        ast.toText(builder)
        assertEquals(source, builder.toString())
    }
}